#ifndef INCLUDED_ROTATINGSTREAM_
#define INCLUDED_ROTATINGSTREAM_

#include <ostream>
#include "../rotatingstreambuf/rotatingstreambuf.h"

class RotatingStream: private RotatingStreambuf, public std::ostream
{
    public:
        RotatingStream(void (*header)(std::ostream &) = 0);

        void open(std::string const &name,  // forwards to RotatingStreambuf
                  bool stdLog);             // true: std logs, false: data log

        static void notify();
};

inline void RotatingStream::open(std::string const &name, bool stdLog)
{
    RotatingStreambuf::open(name, stdLog);
}

inline void RotatingStream::notify()
{
    RotatingStreambuf::notify();
}

#endif

#include "conntrackproducer.ih"

void ConntrackProducer::setTcpUdp(size_t protocolType , char const *label)
{
    if (not d_options.hasProtocol(protocolType))
        return;

    imsg << "conntrack reports " << label << " connections" << endl;
    d_tcpUdp = &ConntrackProducer::tcpUdp;
}

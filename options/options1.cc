#include "options.ih"

// at this time we're not running as a daemon, and all messages are sent to
// cout

Options::Options()
:
    d_arg(ArgConfig::instance()),
    d_rotate(d_arg.option(0, "rotate")),
    d_rotateData(d_arg.option(0, "rotate-data")),
    d_terminate(d_arg.option(0, "terminate")),
    d_rotationInterval(30)
{
    openConfig();

                                        // must be after openConfig because
                                        // config params are used by terminate
                                        // and rotate(Data)
    if (d_terminate or d_rotate or d_rotateData)
        return;

    d_byteCounts = not d_arg.option(0, "no-bytes");
    d_showDst   =  not d_arg.option(0, "no-dst");
    d_showVia   =  not d_arg.option(0, "no-via");

    setMode();                          // conntrack, device, tcpdump

    setProtocol();

    if (d_mode == CONNTRACK)
        setConntrack();

    setLogParams();

    d_verbose = d_arg.option('V');      // not a bool, but a size_t

    setBoolMembers();

    setTimeSpec();
}



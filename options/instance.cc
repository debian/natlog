#include "options.ih"

Options &Options::instance()
{
    if (not s_options)
        s_options.reset(new Options);  
                                    // yes, it exists until the program ends
                                    // and memory is claimed back by the
                                    // operaring system
    return *s_options;
}

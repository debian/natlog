#include "options.ih"

void Options::setProtocol()
{
    string spec;

    if (not d_arg.option(&spec, 'P'))           // by default: use 
        d_protocols.insert(TCP);                // protocol TCP

    else if (spec == "all")                     // for 'all': use TCP, UDP
        d_protocols = { TCP, UDP, ICMP };       // and ICMP

    else                                        // otherwise use the specified
    {                                           // protocol(s)
        size_t pos = 0;
        size_t colon;
        while (true)
        {
            colon = spec.find(':', pos);    

            string protocol = spec.substr(pos, colon - pos);

                                                // only accept known protocols
            auto iter = s_name2protocol.find(protocol);
            if (iter == s_name2protocol.end())  
                fmsg << "protocol `" << protocol << 
                            "' currently not supported" << noid;

            d_protocols.insert(iter->second);

            if (colon == string::npos)
                break;

            pos = colon + 1;
        }

//FBB: NEW: multiple protocols with conntrack now OK
//        if (d_mode == CONNTRACK and pos != 0)
//            fmsg << 
//                "multiple protocols not supported with 'conntrack'" <<
//                noid;
    }
}








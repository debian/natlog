#include "record.ih"

void Record::addReceivedBytes(Record const &next)
{
    // d_receivedBytes += next.payload();
    d_receivedBytes += next.d_receivedBytes;
    
    setEndTime(next);
}


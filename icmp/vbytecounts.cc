#include "icmp.ih"

// overrides
void ICMP::vByteCounts(Record const &record) const
{
    stdMsg() << " "
                "sent: "     << setw(10) << record.sentBytes() << ", "
                "received: " << setw(10) << record.receivedBytes();
}

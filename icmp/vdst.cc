#include "icmp.ih"

// overrides
void ICMP::vDst(Record const &record) const
{
    stdMsg() << " "
                "to " << record.destIPstr() << "; "  "     ";
}

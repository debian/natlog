#include "connectionsconsumer.ih"

void ConnectionsConsumer::header(ostream &log)
{
    log <<          "type," <<

        setw(11) << "srcNr"     << ',' <<
        setw(16) << "srcIP"     << ',' <<
        setw(8)  << "srcPort"   << ',';

    if (Options::instance().showDst())
        log << 
            setw(11) << "dstNr"     << ',' <<
            setw(16) << "dstIP"     << ',' <<
            setw(8)  << "dstPort"   << ',';

    if (Options::instance().showVia())
        log << 
            setw(11) << "viaNr"     << ',' <<
            setw(16) << "viaIP"     << ',' <<
            setw(8)  << "viaPort"   << ',';

    if (Options::instance().byteCounts())
        log <<
            setw(11) << "sent"      << ',' <<
            setw(11) << "recvd"     << ',';

    log <<
        setw(11) << "begin"     << ',' <<
        setw(11) << "end"       << ", " <<

        setw(22) << "beginTime" << ", " <<
        setw(22) << "endTime"   << ",     status" << endl;
                                 //   1234567890
}

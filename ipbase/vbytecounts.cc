#include "ipbase.ih"

void IPbase::vByteCounts(Record const &record) const
{
    d_stdMsg << " "
            "sent: "     << setw(10) << record.sentBytes() << ", "
            "received: " << setw(10) << record.receivedBytes();
}

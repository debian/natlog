#include "ipbase.ih"

void IPbase::viaData(Record const &record) const
{
    d_logDataStream <<
        setw(11) << ntohl(record.viaIP())  << ',' <<
        setw(16) << record.viaIPstr()      << ',' <<
        setw(8)  << record.viaPort()       << ',';
}


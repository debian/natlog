#include "rotatingstreambuf.ih"

// static
void RotatingStreambuf::startThread()
{
    if (rotationsRequested())
        s_rotateThread = thread{ rotateThread };
}

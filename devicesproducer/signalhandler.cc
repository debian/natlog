#include "devicesproducer.ih"

void DevicesProducer::signalHandler(size_t signum)
{
    d_stdMsg << "received signal " << signum << " (" << 
        (signum == SIGINT ? "SIGINT" : "SIGTERM") << ')' << endl;

    d_signaled.notify();
}
